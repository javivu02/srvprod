package com.techuniversity.controllers;


import com.techuniversity.servicios.ServiciosService;
import org.json.JSONObject;
import org.bson.Document;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {
//    @GetMapping("/servicios")
//    public List getServicios(){
//        return ServiciosService.getAll();
//    }

    @PostMapping("/servicios")
    public String setServicios(@RequestBody String cadena){
        try{
            Document doc = Document.parse(cadena);
            List<Document> listServicios = doc.getList("servicios", Document.class);
            if (listServicios != null){
                ServiciosService.insertBatch(cadena);
            }else{
                ServiciosService.insert(cadena);
            }
            return "Ok";
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    @GetMapping("/servicios")
    public List setServicio(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodo(@RequestParam String periodo){
        Document doc = new Document();
        doc.append("disponibilidad.periodos", periodo);
        return ServiciosService.getFiltradosPeriodo(doc);
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String valores = obj.getJSONObject("valores").toString();
            ServiciosService.update(filtro, valores);
            return "Ok";
        }catch (Exception e){
            return e.getMessage();
        }
    }
}

