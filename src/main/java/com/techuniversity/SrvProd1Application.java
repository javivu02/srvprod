package com.techuniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrvProd1Application {

	public static void main(String[] args) {
		SpringApplication.run(SrvProd1Application.class, args);
	}

}
